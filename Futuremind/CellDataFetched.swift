//
//  CellDataFetched.swift
//  Futuremind
//
//  Created by Eryk Sajur on 04/04/2017.
//  Copyright © 2017 Eryk Sajur. All rights reserved.
//

import Foundation
import SwiftyJSON

struct CellDataFetched {
	let title: String
	let desc: String
	let url: String
	let imageUrl: String
	let orderId: Int16
	let modificationDate: Date
}

extension CellDataFetched: JsonArrayDecodable {
	static private let dateFormatter: DateFormatter = {
		let formatter = DateFormatter()
		formatter.dateFormat = "yyy-MM-dd"
		return formatter
	}()

	init(json: JSON) throws {
		guard let title = json["title"].string,
			let wholeDescription = json["description"].string,
			let imageUrl = json["image_url"].string,
			let dateString = json["modificationDate"].string,
			let date = CellDataFetched.dateFormatter.date(from: dateString),
			let orderId = json["orderId"].int
			else { throw FuturemindError.parsing(json) }

		self.title = title
		let (url, description) = CellDataFetched.urlAndDescription(from: wholeDescription)
		self.desc = description
		self.url = url
		self.imageUrl = imageUrl
		self.modificationDate = date
		self.orderId = Int16(orderId)
	}

	static private func urlAndDescription(from combined: String) -> (String, String) {
		var components: [String] = combined.components(separatedBy: "http")
		let description = components[0].trimmingCharacters(in: .whitespaces)
		let url = "http" + components[1]
		return (url, description)
	}
}
