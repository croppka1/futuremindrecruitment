//
//  CellViewModel.swift
//  Futuremind
//
//  Created by Eryk Sajur on 04/04/2017.
//  Copyright © 2017 Eryk Sajur. All rights reserved.
//

import Foundation
import UIKit

class CellViewModel {

	private static let dateFormatter: DateFormatter = {
		let formatter: DateFormatter = DateFormatter()
		formatter.dateFormat = "yyyy-MM-dd"
		return formatter
	}()

	var apiConnector: ApiConnecting = ApiConnector()

	var title: String
	var description: String
	var url: String
	var imageUrl: String
	var modificationDate: String

	init(title: String, description: String, url: String, imageUrl: String, modificationDate: Date) {
		self.title = title
		self.description = description
		self.url = url
		self.imageUrl = imageUrl
		self.modificationDate = CellViewModel.dateFormatter.string(from: modificationDate)
	}

	func fetchImage(completion: @escaping (UIImage?) -> Void) {
		let endpoint = FuturemindApiEndpoint.custom(fullPath: imageUrl)
		apiConnector.getData(from: endpoint) { [weak self] (dataFetchResult) in
			if case .success(let data) = dataFetchResult {
				let image = UIImage(data: data)
				completion(image)
			} else {
				completion(nil)
			}
		}
	}
}

extension DataTableViewCell {
	func set(_ viewModel: CellViewModel) {
		titleLabel.text = viewModel.title
		descriptionLabel.text = viewModel.description
		modificationDateLabel.text = viewModel.modificationDate
		self.viewModel = viewModel
		viewModel.fetchImage { [weak self] (image) in
			if self?.viewModel === viewModel {
				DispatchQueue.main.async {
					self?.contentImageView.image = image
				}
			}
		}
	}
}
