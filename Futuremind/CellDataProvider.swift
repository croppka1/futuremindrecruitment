//
//  CellDataProvider.swift
//  Futuremind
//
//  Created by Eryk Sajur on 04/04/2017.
//  Copyright © 2017 Eryk Sajur. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol CellDataProviding: class {
	var onDataRefresh: (() -> Void)? { get set }
	var numberOfCellsDataKinds: Int { get }
	func numberOfCellData(in section: Int) -> Int
	func cellDataViewModel(at indexPath: IndexPath) -> CellViewModel
	func cellDataUrl(at indexPath: IndexPath) -> String
}

class CellDataProvider: NSObject, CellDataProviding {

	//MARK : Dependencies
	let context: NSManagedObjectContext
	lazy var fetchedResultsController: NSFetchedResultsController<CellData> = {
		let request: NSFetchRequest<CellData> = NSFetchRequest<CellData>(entityName: "CellData")
		request.sortDescriptors = [NSSortDescriptor(key: "orderId", ascending: true) ]
		let frc = NSFetchedResultsController(
			fetchRequest: request,
			managedObjectContext: self.context,
			sectionNameKeyPath: nil,
			cacheName: nil)
		frc.delegate = self
		let _ = try? frc.performFetch()
		return frc
	}()

	//MARK : Public properties
	var onDataRefresh: (() -> Void)?

	// MARK : Initializers
	init(context: NSManagedObjectContext) {
		self.context = context
		self.context.automaticallyMergesChangesFromParent = true
	}

	// MARK: Public API
	var numberOfCellsDataKinds: Int {
		return 1
	}

	func numberOfCellData(in section: Int) -> Int {
		return fetchedResultsController.sections?[section].objects?.count ?? 0
	}

	func cellDataViewModel(at indexPath: IndexPath) -> CellViewModel {
		let cellData: CellData = fetchedResultsController.object(at: indexPath) 
		return CellViewModel(title: cellData.title!,
		              description: cellData.desc!,
		              url: cellData.url!,
		              imageUrl: cellData.imageUrl!,
		              modificationDate: cellData.modificationDate! as Date
		)
	}

	func cellDataUrl(at indexPath: IndexPath) -> String {
		return fetchedResultsController.object(at: indexPath).url!
	}
}

extension CellDataProvider: NSFetchedResultsControllerDelegate {
	func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
		onDataRefresh?() // Tutaj oczywiście powinny być obsłużone różne przypadki
	}
}
