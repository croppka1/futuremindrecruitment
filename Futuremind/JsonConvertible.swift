//
//  JsonConvertible.swift
//  Futuremind
//
//  Created by Eryk Sajur on 04/04/2017.
//  Copyright © 2017 Eryk Sajur. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol JsonDecodable {
	init(json: JSON) throws
}

protocol JsonArrayDecodable: JsonDecodable  {
	static func parseAll(_ json: JSON) throws -> [Self]
}

extension JsonArrayDecodable {
	static func parseAll(_ json: JSON) throws -> [Self] {
		guard let array = json.array else {
			throw FuturemindError.parsing(json)
		}
		return try array.map { try Self(json: $0) }
	}
}
