//
//  Errors.swift
//  Futuremind
//
//  Created by Eryk Sajur on 04/04/2017.
//  Copyright © 2017 Eryk Sajur. All rights reserved.
//

import Foundation

enum FuturemindError: Error {
	case parsing(Any)
	case unknown
	case server(code: Int, message: String)
}

extension FuturemindError: LocalizedError {
	var errorDescription: String? {
		switch self {
		case .parsing(let data):
			return "Failed to parse \(data)"
		case .unknown:
			return "Unknown error"
		case .server(let code, let message):
			return "\(code): " + message
		}
	}

	static func validate(data: Data?, response: URLResponse?, error: Error?) -> FuturemindError? {
		guard let response = response as? HTTPURLResponse else { return FuturemindError.unknown }
		switch response.statusCode {
		case (200...300):
			return nil
		default:
			let message = self.parseMessage(from: data)
			return .server(code: response.statusCode, message: message)
		}
	}

	static func parseMessage(from data: Data?) -> String {
		return "The response was invalid :)"
	}
}


