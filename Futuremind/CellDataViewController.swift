//
//  ViewController.swift
//  Futuremind
//
//  Created by Eryk Sajur on 04/04/2017.
//  Copyright © 2017 Eryk Sajur. All rights reserved.
//

import UIKit
import SnapKit

class CellDataViewController: UIViewController {

	let viewModel: CellDataViewModeling
	let refreshControl: UIRefreshControl = UIRefreshControl()
	lazy var tableView: UITableView = UITableView()

	init(viewModel: CellDataViewModeling) {
		self.viewModel = viewModel
		super.init(nibName: nil, bundle: nil)
	}

	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func loadView() {
		view = UIView()
		setup()
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		layout()
	}

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		viewModel.prepareToAppear()
	}
}

extension CellDataViewController { // layout
	func layout() {
		layoutTableView()
	}

	func layoutTableView() {
		view.addSubview(tableView)
		tableView.snp.makeConstraints { make in
			make.leading.trailing.equalToSuperview()
			make.top.equalTo(topLayoutGuide.snp.bottom)
			make.bottom.equalTo(bottomLayoutGuide.snp.top)
		}
	}
}

extension CellDataViewController { //setup
	fileprivate func setup() {
		setupTableView()
		setupViewModel()
	}

	private func setupTableView(){
		automaticallyAdjustsScrollViewInsets = false
		tableView.estimatedRowHeight = 10.0
		tableView.rowHeight = UITableViewAutomaticDimension
		tableView.delegate = self
		tableView.refreshControl = refreshControl
		refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
		viewModel.setup(tableView)
	}

	private func setupViewModel() {
		viewModel.onUrlPresent = { [weak self] in
			let viewController = WebViewController(url: $0)
			self?.navigationController?.pushViewController(viewController, animated: true)
		}
	}

	@objc private func refresh() {
		viewModel.refreshData { [weak self] in
			DispatchQueue.main.async {
				self?.refreshControl.endRefreshing()
			}
		}
	}
}

extension CellDataViewController: UITableViewDelegate {
	func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableViewAutomaticDimension
	}

	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: false)
		viewModel.handleSelection(at: indexPath)
	}
}
