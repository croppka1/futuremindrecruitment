//
//  WebViewController.swift
//  Futuremind
//
//  Created by Eryk Sajur on 04/04/2017.
//  Copyright © 2017 Eryk Sajur. All rights reserved.
//

import Foundation
import UIKit

class WebViewController: UIViewController {
	let savedURL: URL
	var webView: UIWebView { return view as! UIWebView }

	init(url: URL) {
		savedURL = url
		super.init(nibName: nil, bundle: nil)
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func loadView() {
		view = UIWebView()
	}

	override func viewWillAppear(_ animated: Bool) {
		let request = URLRequest(url: savedURL)
		webView.loadRequest(request)
	}
}
