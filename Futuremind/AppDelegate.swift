//
//  AppDelegate.swift
//  Futuremind
//
//  Created by Eryk Sajur on 04/04/2017.
//  Copyright © 2017 Eryk Sajur. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

	var window: UIWindow?
	let navigationController: UINavigationController = UINavigationController()
	let coreDataStack: CoreDataStack = CoreDataStack()

	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
		// Override point for customization after application launch.
		window = UIWindow(frame: UIScreen.main.bounds)
		window?.rootViewController = navigationController
		setupRootViewController()
		window?.makeKeyAndVisible()
		return true
	}

	func setupRootViewController() {
		let container = coreDataStack.persistentContainer
		let viewModel: CellDataViewModeling = CellDataViewModel(persistentContainer: container)
		let viewController: CellDataViewController = CellDataViewController(viewModel: viewModel)
		navigationController.setViewControllers([viewController], animated: false)
	}
}

