//
//  CellDataSaver.swift
//  Futuremind
//
//  Created by Eryk Sajur on 04/04/2017.
//  Copyright © 2017 Eryk Sajur. All rights reserved.
//

import Foundation
import CoreData

protocol CellDataSaving: class {
	func save(fetchedData: [CellDataFetched])
}

class CellDataSaver: CellDataSaving {
	let parsingContext: NSManagedObjectContext

	init(parsingContext: NSManagedObjectContext) {
		self.parsingContext = parsingContext
	}

	func save(fetchedData: [CellDataFetched]) {
		do {
			for data in fetchedData {
				let predicate: NSPredicate = NSPredicate(format: "SELF.orderId == %d", data.orderId)
				let request: NSFetchRequest<CellData> = NSFetchRequest<CellData>(entityName: "CellData")
				request.predicate = predicate

				if let saved: CellData = try parsingContext.fetch(request).first {
					insert(data, into: saved)
				} else {
					guard let new = NSEntityDescription.insertNewObject(forEntityName: "CellData", into: parsingContext) as? CellData else { throw FuturemindError.parsing(data) }
					insert(data, into: new)
				}
			}
			try parsingContext.save()
		} catch {
			print("Error needs to be handled", error)
		}
	}

	private func insert(_ fetchedData: CellDataFetched, into cellData: CellData) {
		cellData.orderId = fetchedData.orderId
		cellData.title = fetchedData.title
		cellData.desc = fetchedData.desc
		cellData.url = fetchedData.url
		cellData.imageUrl = fetchedData.imageUrl
		cellData.modificationDate = fetchedData.modificationDate as NSDate
	}
}
