//
//  CellDataViewModel.swift
//  Futuremind
//
//  Created by Eryk Sajur on 04/04/2017.
//  Copyright © 2017 Eryk Sajur. All rights reserved.
//

import Foundation
import UIKit
import CoreData

protocol CellDataViewModeling: class, UITableViewDataSource {
	var onUrlPresent: ((URL) -> ())? { get set }
	func setup(_ tableView: UITableView)
	func handleSelection(at indexPath: IndexPath)
	func prepareToAppear()
	func refreshData(completion: @escaping() -> Void)
}

class CellDataViewModel: NSObject, CellDataViewModeling {
	// MARK : Dependencies
	let persistentContainer: NSPersistentContainer
	var cellDataFetcher: CellDataFetching = CellDataFetcher()
	var cellDataSaver: CellDataSaving
	var cellDataProvider: CellDataProviding

	//MARK : Public properties
	var tableViewCellReuseIdentifier: String { return "reuse_identifier" }
	var onUrlPresent: ((URL) -> ())?

	// MARK : Initializers
	init(persistentContainer: NSPersistentContainer) {
		self.persistentContainer = persistentContainer
		self.cellDataSaver = CellDataSaver(
			parsingContext: persistentContainer.newBackgroundContext()
		)
		self.cellDataProvider = CellDataProvider(
			context: persistentContainer.viewContext
		)
		super.init()
	}

	// MARK : Public API
	func prepareToAppear() {
		fetchData()
	}

	func setup(_ tableView: UITableView) {
		tableView.dataSource = self
		tableView.register(DataTableViewCell.self,
		                   forCellReuseIdentifier: tableViewCellReuseIdentifier)
		cellDataProvider.onDataRefresh = {
			tableView.reloadData()
		}
	}

	func handleSelection(at indexPath: IndexPath) {
		let urlString = cellDataProvider.cellDataUrl(at: indexPath)
		let url = URL(string: urlString)!
		onUrlPresent?(url)
	}

	func refreshData(completion: @escaping () -> Void) {
		fetchData(completion: completion)
	}

	// MARK : Private methods
	private func fetchData(completion: @escaping () -> Void = { }) {
		cellDataFetcher.fetch { [weak self] (result) in
			switch result {
			case .success(let data):
				self?.save(data)
			case .failure(let error):
				print("error occured", error)
			}
			completion()
		}
	}

	private func save(_ data: [CellDataFetched]) {
		cellDataSaver.save(fetchedData: data)
	}
}

extension CellDataViewModel: UITableViewDataSource {
	func numberOfSections(in tableView: UITableView) -> Int {
		return cellDataProvider.numberOfCellsDataKinds
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return cellDataProvider.numberOfCellData(in: section)
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell: DataTableViewCell? = tableView.dequeueReusableCell(withIdentifier: tableViewCellReuseIdentifier, for: indexPath) as? DataTableViewCell
		let viewModel = cellDataProvider.cellDataViewModel(at: indexPath)
		cell?.set(viewModel)

		return cell ?? UITableViewCell()
	}
}
