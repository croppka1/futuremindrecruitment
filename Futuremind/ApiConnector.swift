//
//  ApiConnector.swift
//  Futuremind
//
//  Created by Eryk Sajur on 04/04/2017.
//  Copyright © 2017 Eryk Sajur. All rights reserved.
//

import Foundation
import SwiftyJSON

enum FetchResult<T> {
	case success(data: T)
	case failure(error: Error)
}

protocol ApiConnecting: class {
	func getJson(from endpoint: ApiEndpoint, completion: @escaping (FetchResult<JSON>) -> Void)
	func getData(from endpoint: ApiEndpoint, completion: @escaping (FetchResult<Data>) -> Void)
}

class ApiConnector: ApiConnecting {

	static private let session: URLSession = URLSession(configuration: .default)

	func getJson(from endpoint: ApiEndpoint, completion: @escaping (FetchResult<JSON>) -> Void) {
		let path = endpoint.fullPath
		let url = URL(string: path)!
		let task = ApiConnector.session.dataTask(with: url, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
			let result: FetchResult<JSON>
			do {
				if let error = FuturemindError.validate(data: data, response: response, error: error) {
					throw error
				}
				guard let data = data else { throw FuturemindError.unknown }
				result = .success(data: JSON(data: data))

			} catch {
				result = .failure(error: error)
			}
			completion(result)
		})
		task.resume()
	}

	func getData(from endpoint: ApiEndpoint, completion: @escaping (FetchResult<Data>) -> Void) {
		let path = endpoint.fullPath
		let url = URL(string: path)!
		let task = ApiConnector.session.dataTask(with: url, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) in
			let result: FetchResult<Data>
			do {
				if let error = FuturemindError.validate(data: data, response: response, error: error) {
					throw error
				}
				guard let data = data else { throw FuturemindError.unknown }
				result = .success(data: data)

			} catch {
				result = .failure(error: error)
			}
			completion(result)
		})
		task.resume()
	}
}
