//
//  CellDataFetcher.swift
//  Futuremind
//
//  Created by Eryk Sajur on 04/04/2017.
//  Copyright © 2017 Eryk Sajur. All rights reserved.
//

import Foundation

protocol CellDataFetching: class {
	func fetch(completion: @escaping (FetchResult<[CellDataFetched]>) -> Void)
}

class CellDataFetcher: CellDataFetching {
	var apiConnector: ApiConnecting = ApiConnector()


	func fetch(completion: @escaping (FetchResult<[CellDataFetched]>) -> Void) {
		apiConnector.getJson(from: FuturemindApiEndpoint.cellData) { (fetchResult) in
			let result: FetchResult<[CellDataFetched]>
			do {
				switch fetchResult {
				case .success(let json):
					let cellData: [CellDataFetched] = try CellDataFetched.parseAll(json["data"])
					result = .success(data: cellData)
				case .failure(let error):
					throw error
				}
			} catch {
				result = .failure(error: error)
			}
			completion(result)
		}
	}
}
