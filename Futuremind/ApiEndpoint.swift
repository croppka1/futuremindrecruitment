//
//  ApiEndpoint.swift
//  Futuremind
//
//  Created by Eryk Sajur on 04/04/2017.
//  Copyright © 2017 Eryk Sajur. All rights reserved.
//

import Foundation

let BaseURL: String = "http://pinky.futuremind.com/~dpaluch/"

protocol ApiEndpoint {
	var fullPath: String { get }
}

enum FuturemindApiEndpoint: ApiEndpoint {

	case cellData
	case custom(fullPath: String)

	var urlPart: String {
		switch self {
		case .cellData:		return "test35"
		case.custom(_): return ""
		}
	}

	var fullPath: String {
		switch self{
		case .custom(let path):
			return path
		default:
			return BaseURL + self.urlPart
		}
	}
}
