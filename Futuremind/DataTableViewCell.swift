//
//  DataTableViewCell.swift
//  Futuremind
//
//  Created by Eryk Sajur on 04/04/2017.
//  Copyright © 2017 Eryk Sajur. All rights reserved.
//

import Foundation
import UIKit

class DataTableViewCell: UITableViewCell {

	var viewModel: CellViewModel?
	var titleLabel: UILabel = UILabel()
	var descriptionLabel: UILabel = UILabel()
	var modificationDateLabel: UILabel = UILabel()
	var contentImageView: UIImageView = UIImageView()

	override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
		super.init(style: style, reuseIdentifier: reuseIdentifier)
		layout()
	}
	
	required init?(coder aDecoder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	override func prepareForReuse() {
		super.prepareForReuse()
		contentImageView.image = nil
	}
}

extension DataTableViewCell { // layout
	func layout() {
		let titleContainer: UIView = UIView()
		titleLabel.numberOfLines = 0
		descriptionLabel.numberOfLines = 0
		titleLabel.font = UIFont.boldSystemFont(ofSize: 22.0)
		descriptionLabel.font = UIFont.systemFont(ofSize: 11.0)
		modificationDateLabel.font = UIFont.systemFont(ofSize: 9.0)
		contentView.addSubview(titleContainer)
		titleContainer.addSubview(titleLabel)
		contentView.addSubview(descriptionLabel)
		contentView.addSubview(contentImageView)
		contentView.addSubview(modificationDateLabel)
		let imageSize = CGSize(width: 100.0, height: 100.0)
		let sideOffset = 15.0
		let descriptionOffset = 6.0

		contentImageView.snp.makeConstraints { make in
			make.leading.top.equalToSuperview().offset(sideOffset)
			make.size.equalTo(imageSize)
		}

		modificationDateLabel.snp.makeConstraints { make in
			make.top.equalTo(contentImageView)
			make.leading.equalTo(contentImageView.snp.trailing).offset(sideOffset)
		}

		titleContainer.snp.makeConstraints { make in
			make.leading.equalTo(contentImageView.snp.trailing)
			make.top.equalTo(modificationDateLabel.snp.bottom)
			make.trailing.equalToSuperview().offset(-sideOffset)
			make.bottom.equalTo(contentImageView.snp.bottom)
		}

		titleLabel.snp.makeConstraints { make in
			make.center.equalToSuperview()
			make.trailing.lessThanOrEqualToSuperview()
			make.leading.greaterThanOrEqualToSuperview()
			make.top.greaterThanOrEqualToSuperview()
			make.bottom.lessThanOrEqualToSuperview()
		}

		descriptionLabel.snp.makeConstraints { make in
			make.leading.greaterThanOrEqualToSuperview().offset(sideOffset)
			make.trailing.lessThanOrEqualToSuperview().offset(-sideOffset)
			make.top.greaterThanOrEqualTo(contentImageView.snp.bottom).offset(descriptionOffset)
			make.top.greaterThanOrEqualTo(titleLabel.snp.bottom).offset(descriptionOffset)
			make.top.equalTo(contentImageView).offset(descriptionOffset).priority(100)
			make.bottom.equalToSuperview()
		}
	}
}
